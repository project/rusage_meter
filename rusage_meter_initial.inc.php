<?php

/**
 * @file
 * Sets the initial rusage data when the autoloader is not initialized yet.
 */

use Drupal\rusage_meter\RusageMeter;

require_once 'src/RusageMeter.php';

global $_rusage_meter_initial_data;
if ($_rusage_meter_initial_data === NULL) {
  $_rusage_meter_initial_data = RusageMeter::collectData();
}
