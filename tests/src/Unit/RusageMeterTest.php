<?php

namespace Drupal\Tests\rusage_meter\Unit;

use Drupal\rusage_meter\RusageMeter;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\rusage_meter\RusageMeter
 * @group rusage_meter
 */
class RusageMeterTest extends UnitTestCase {

  /**
   * @covers ::collectData
   */
  public function testCollectData() {
    $data = RusageMeter::collectData();
    $this->assertArrayHasKey(RusageMeter::KEY_MICROTIME, $data);
    $this->assertArrayHasKey(RusageMeter::KEY_MEMORY_USAGE, $data);
    $this->assertArrayHasKey(RusageMeter::KEY_MEMORY_PEAK_USAGE, $data);
  }

}
