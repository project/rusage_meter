<?php

namespace Drupal\Tests\rusage_meter\Unit;

use Drupal\rusage_meter\RusageMeter;
use Drupal\rusage_meter\RusageMeterService;
use Drupal\test_helpers\TestHelpers;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @coversDefaultClass \Drupal\rusage_meter\RusageMeterService
 * @group rusage_meter
 */
class RusageMeterServiceTest extends UnitTestCase {

  /**
   * @covers ::__construct
   * @covers ::putInitialData
   */
  public function testConstruct() {
    $service = TestHelpers::initService(RusageMeterService::class, 'rusage_meter');
    $this->checkRusageDataArray(TestHelpers::getPrivateProperty($service, 'initialData'));
  }

  /**
   * @covers ::__construct
   */
  public function testGetSubscribedEvents() {
    $events = RusageMeterService::getSubscribedEvents();
    $this->assertEquals([
      KernelEvents::REQUEST => ['putInitialData', 1000],
      KernelEvents::VIEW => ['putInitialData', 1000],
      KernelEvents::RESPONSE => ['injectRusageData', -1000],
    ], $events);

    $kernel = $this->createMock(HttpKernelInterface::class);
    $request = TestHelpers::service('request_stack')->getCurrentRequest();
    $event = new RequestEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST);
    TestHelpers::callEventSubscriber('rusage_meter', KernelEvents::REQUEST, $event);
    TestHelpers::callEventSubscriber('rusage_meter', KernelEvents::VIEW, $event);
  }

  /**
   * @covers ::setInitialData
   * @covers ::setFinalData
   */
  public function testSetInitialData() {
    $service = new RusageMeterService();
    $this->checkRusageDataArray(TestHelpers::getPrivateProperty($service, 'initialData'));
    $service->setFinalData();
    $this->checkRusageDataArray(TestHelpers::getPrivateProperty($service, 'finalData'));

    $testData1 = ['foo' => 'bar'];
    $testData2 = ['baz' => 'qux'];
    $testData3 = ['fred' => 'thud'];
    global $_rusage_meter_initial_data;
    global $_rusage_meter_final_data;

    $_rusage_meter_initial_data = $testData1;
    $service = new RusageMeterService();
    $service->setFinalData();
    $this->assertEquals($testData1, TestHelpers::getPrivateProperty($service, 'initialData'));
    $this->checkRusageDataArray(TestHelpers::getPrivateProperty($service, 'finalData'));

    $_rusage_meter_initial_data = NULL;
    $_rusage_meter_final_data = $testData1;
    $service = new RusageMeterService();
    $service->setFinalData();
    $this->checkRusageDataArray(TestHelpers::getPrivateProperty($service, 'initialData'));
    $this->assertEquals($testData1, TestHelpers::getPrivateProperty($service, 'finalData'));

    $_rusage_meter_initial_data = $testData1;
    $_rusage_meter_final_data = $testData2;
    $service = new RusageMeterService();
    $service->setFinalData();
    $this->assertEquals($testData1, TestHelpers::getPrivateProperty($service, 'initialData'));
    $this->assertEquals($testData2, TestHelpers::getPrivateProperty($service, 'finalData'));

    $service->setInitialData($testData3);
    $service->setFinalData($testData3);
    $this->assertEquals($testData1, TestHelpers::getPrivateProperty($service, 'initialData'));
    $this->assertEquals($testData2, TestHelpers::getPrivateProperty($service, 'finalData'));

    $service->setInitialData($testData3, TRUE);
    $service->setFinalData($testData2, TRUE);
    $this->assertEquals($testData3, TestHelpers::getPrivateProperty($service, 'initialData'));
    $this->assertEquals($testData2, TestHelpers::getPrivateProperty($service, 'finalData'));

    $_rusage_meter_initial_data = NULL;
    $_rusage_meter_final_data = NULL;
  }

  /**
   * @covers ::injectRusageData
   * @covers ::addResponseHeaders
   * @covers ::getDiff
   * @covers ::timeAsFloat
   * @covers ::isEnabled
   */
  public function testInjectRusageData() {
    $this->assertArrayNotHasKey(RusageMeterService::HEADER_UTIME, $this->getResponseEventHeaders());

    putenv(RusageMeterService::ENV_INJECT_ALWAYS . '=1');
    $this->assertTrue($this->getResponseEventHeaders()[RusageMeterService::HEADER_UTIME] > 0);

    putenv(RusageMeterService::ENV_INJECT_ALWAYS . '=0');
    $this->assertArrayNotHasKey(RusageMeterService::HEADER_UTIME, $this->getResponseEventHeaders());

    putenv(RusageMeterService::ENV_INJECT_KEY . '=foo');
    $this->assertArrayNotHasKey(RusageMeterService::HEADER_UTIME, $this->getResponseEventHeaders());

    $request = new Request([RusageMeterService::ENV_INJECT_QUERY_PARAMETER => 'foo']);
    $this->assertTrue($this->getResponseEventHeaders($request)[RusageMeterService::HEADER_UTIME] > 0);

    $request = new Request([RusageMeterService::ENV_INJECT_QUERY_PARAMETER => 'bar']);
    $this->assertArrayNotHasKey(RusageMeterService::HEADER_UTIME, $this->getResponseEventHeaders($request));

    $request = new Request();
    $request->headers->set(RusageMeterService::PREFIX_HEADER . RusageMeterService::ENV_INJECT_QUERY_PARAMETER, 'foo');
    $this->assertTrue($this->getResponseEventHeaders($request)[RusageMeterService::HEADER_UTIME] > 0);

    $request = new Request();
    $request->headers->set(RusageMeterService::PREFIX_HEADER . RusageMeterService::ENV_INJECT_QUERY_PARAMETER, 'bar');
    $this->assertArrayNotHasKey(RusageMeterService::HEADER_UTIME, $this->getResponseEventHeaders($request));
  }

  /**
   * Calls the KernelEvents::RESPONSE event and return headers.
   */
  private function getResponseEventHeaders(Request $request = NULL) {
    $kernel = $this->createMock(HttpKernelInterface::class);
    $request ??= TestHelpers::service('request_stack')->getCurrentRequest();
    $response = new Response();
    $event = new ResponseEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST, $response);
    TestHelpers::callEventSubscriber('rusage_meter', KernelEvents::RESPONSE, $event);
    return $event->getResponse()->headers->all();
  }

  /**
   * Checks if the rusage_meter data array has valid data.
   *
   * @param array $data
   *   An array in rusage_meter format.
   */
  private function checkRusageDataArray(array $data) {
    $this->assertIsNumeric($data['ru_stime.tv_usec']);
    $this->assertIsNumeric($data[RusageMeter::KEY_MICROTIME]);
    $this->assertIsNumeric($data[RusageMeter::KEY_MEMORY_USAGE]);
    $this->assertIsNumeric($data[RusageMeter::KEY_MEMORY_PEAK_USAGE]);
  }

}
