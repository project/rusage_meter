<?php

namespace Drupal\rusage_meter;

/**
 * Utility functions to collect the resource usage data without dependencies.
 */
class RusageMeter {

  /**
   * A key to store the microtime data.
   *
   * @var string
   */
  const KEY_MICROTIME = '_microtime';
  /**
   * A key to store the memory_usage data.
   *
   * @var string
   */
  const KEY_MEMORY_USAGE = '_memory_usage';
  /**
   * A key to store the memory_peak_usage data.
   *
   * @var string
   */
  const KEY_MEMORY_PEAK_USAGE = '_memory_peak_usage';

  /**
   * Collects the resources usage data.
   */
  public static function collectData(): array {
    $data = getrusage();
    $data[self::KEY_MICROTIME] = microtime(TRUE);
    $data[self::KEY_MEMORY_USAGE] = memory_get_usage(TRUE);
    $data[self::KEY_MEMORY_PEAK_USAGE] = memory_get_peak_usage(TRUE);
    return $data;
  }

}
