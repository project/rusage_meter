<?php

namespace Drupal\rusage_meter;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribes to kernel events to initialize the root span.
 */
class RusageMeterService implements EventSubscriberInterface {

  /**
   * An environment variable name to always injecting the rusage headers.
   *
   * @var string
   */
  const ENV_INJECT_ALWAYS = 'RUSAGE_METER_INJECT_ALWAYS';

  /**
   * An environment variable name to set the key that enables injecting.
   *
   * @var string
   */
  const ENV_INJECT_KEY = 'RUSAGE_METER_INJECT_KEY';

  /**
   * An environment variable name to set the key that enables injecting.
   *
   * @var string
   */
  const ENV_INJECT_QUERY_PARAMETER = 'rusage-meter-key';

  /**
   * Header names for specific data.
   */
  const PREFIX_HEADER = 'x-';

  const HEADER_RESPONSE_TIME = self::PREFIX_HEADER . 'rusage-rtime';
  const HEADER_UTIME = self::PREFIX_HEADER . 'rusage-utime';
  const HEADER_STIME = self::PREFIX_HEADER . 'rusage-stime';
  const HEADER_MEMORY_USAGE = self::PREFIX_HEADER . 'rusage-memory';
  const HEADER_MEMORY_PEAK_USAGE = self::PREFIX_HEADER . 'rusage-memory-peak';
  const HEADER_INBLOCK = self::PREFIX_HEADER . 'rusage-inblock';
  const HEADER_OUBLOCK = self::PREFIX_HEADER . 'rusage-oublock';

  /**
   * The initial rusage data in the `getrusage()` format.
   *
   * @var array
   */
  protected array $initialData;

  /**
   * The final rusage data in the `getrusage()` format.
   *
   * @var array
   */
  protected array $finalData;

  /**
   * Constructs a RusageMeterEventSubscriber object with setting the status.
   */
  public function __construct() {
    // Gathering initial data as soon as the service is activated.
    $this->setInitialData();
  }

  /**
   * {@inheritdoc}
   *
   * @internal
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['putInitialData', 1000],
      KernelEvents::VIEW => ['putInitialData', 1000],
      KernelEvents::RESPONSE => ['injectRusageData', -1000],
    ];
  }

  /**
   * Sets the initial rusage data.
   *
   * @param array $data
   *   The data array in the getrusage() format.
   * @param bool $override
   *   A flag to override existing data, if already set.
   */
  public function setInitialData(array $data = NULL, bool $override = FALSE): void {
    global $_rusage_meter_initial_data;
    if (
      $_rusage_meter_initial_data !== NULL
      && !isset($this->initialData)
    ) {
      $this->initialData = $_rusage_meter_initial_data;
    }

    if (!isset($this->initialData) || $override) {
      if ($data === NULL) {
        $data = RusageMeter::collectData();
      }
      $this->initialData = $data;
    }
  }

  /**
   * Sets the final rusage data.
   *
   * @param array $data
   *   The data array in the getrusage() format.
   * @param bool $override
   *   A flag to override existing data, if already set.
   */
  public function setFinalData(array $data = NULL, bool $override = FALSE): void {
    global $_rusage_meter_final_data;
    if (
      $_rusage_meter_final_data !== NULL
      && !isset($this->finalData)
    ) {
      $this->finalData = $_rusage_meter_final_data;
    }

    if (!isset($this->finalData) || $override) {
      if ($data === NULL) {
        $data = RusageMeter::collectData();
      }
      $this->finalData = $data;
    }
  }

  /**
   * Puts the initial rusage statistics for the REQUEST or VIEW event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   A kernel event.
   *
   * @internal
   */
  public function putInitialData($event) {
    $this->setInitialData();
  }

  /**
   * Collects the rusage statistics for the RESPONSE event.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   A ResponseEvent.
   *
   * @internal
   */
  public function injectRusageData(ResponseEvent $event): void {
    if (!$this->isEnabled($event->getRequest())) {
      return;
    }
    $this->setFinalData();
    $this->addResponseHeaders($event->getResponse());
  }

  /**
   * Adds the rusage headers to a response.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   A response.
   */
  private function addResponseHeaders(Response $response): void {
    $response->headers->set(self::HEADER_RESPONSE_TIME, round($this->finalData[RusageMeter::KEY_MICROTIME] - $this->initialData[RusageMeter::KEY_MICROTIME], 6));
    $response->headers->set(self::HEADER_UTIME, $this->getDiff($this->initialData, $this->finalData, 'ru_utime'));
    $response->headers->set(self::HEADER_STIME, $this->getDiff($this->initialData, $this->finalData, 'ru_stime'));
    $response->headers->set(self::HEADER_MEMORY_USAGE, $this->finalData[RusageMeter::KEY_MEMORY_USAGE] - $this->initialData[RusageMeter::KEY_MEMORY_USAGE]);
    $response->headers->set(self::HEADER_MEMORY_PEAK_USAGE, $this->finalData[RusageMeter::KEY_MEMORY_PEAK_USAGE] - $this->initialData[RusageMeter::KEY_MEMORY_PEAK_USAGE]);

    // The ru_inblock and ru_oublock is not available on all OS, so we need to
    // put the 'n/a' for such cases.
    $response->headers->set(self::HEADER_INBLOCK, isset($this->initialData['ru_inblock']) ? ($this->finalData['ru_inblock'] - $this->initialData['ru_inblock']) : 'n/a');
    $response->headers->set(self::HEADER_OUBLOCK, isset($this->initialData['ru_oublock']) ? ($this->finalData['ru_oublock'] - $this->initialData['ru_oublock']) : 'n/a');
  }

  /**
   * Checks if the injection is enabled.
   */
  private function isEnabled(Request $request): bool {
    $enabled = getenv(self::ENV_INJECT_ALWAYS) == TRUE;

    // If not enabled always but the key is configured, check the get parameter
    // in the request query and headers.
    if (!$enabled && $key = getenv(self::ENV_INJECT_KEY)) {
      if (
        $key == $request->query->get(self::ENV_INJECT_QUERY_PARAMETER)
        || $key == $request->headers->get(self::PREFIX_HEADER . self::ENV_INJECT_QUERY_PARAMETER)
      ) {
        $enabled = TRUE;
      }
    }

    return $enabled;
  }

  /**
   * Calculate a time spent on the full response generation.
   *
   * @param array $initial
   *   An array with the initial data in getrusage() format.
   * @param array $final
   *   An array with the final data in getrusage() format.
   * @param string $type
   *   The time type: utime (user time) or stime (system time).
   *
   * @return float
   *   The time in seconds.
   */
  private function getDiff(array $initial, array $final, string $type) {
    return $this->timeAsFloat($final["$type.tv_sec"], $final["$type.tv_usec"])
      - $this->timeAsFloat($initial["$type.tv_sec"], $initial["$type.tv_usec"]);
  }

  /**
   * Converts a time from seconds and microseconds to a float value.
   *
   * @param int $seconds
   *   The amount of seconds.
   * @param int $microseconds
   *   The amount of microseconds.
   *
   * @return float
   *   The float representation of the time.
   */
  private function timeAsFloat(int $seconds, int $microseconds) {
    return $seconds + $microseconds / 1_000_000;
  }

}
